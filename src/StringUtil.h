#ifndef HANGMAN_STRINGUTIL_H
#define HANGMAN_STRINGUTIL_H

#include <string.h>
#include <iostream>

using namespace std;

class StringUtil {

public:
    static bool areequals(char a, char b);

    static bool areequals(const string &a, const string &b);
};

#endif