#ifndef HANGMAN_HANGMAN_H
#define HANGMAN_HANGMAN_H

#include <string.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "StringUtil.h"

using namespace std;

class Hangman {

private:
    static constexpr const char *HEAD = "◔_◔";
    static constexpr const char *NECK = ".";
    static constexpr const char *RIGHT_ARM = "\\";
    static constexpr const char *LEFT_ARM = "/";
    static constexpr const char *TOP_BODY = "|";
    static constexpr const char *BOTTOM_BODY = "|";
    static constexpr const char *RIGHT_FOOT = "\\";
    static constexpr const char *LEFT_FOOT = "/";

    static const int KILL = 0;
    static const int KILL_LEFT_FOOT = 1;
    static const int KILL_RIGHT_FOOT = 2;
    static const int KILL_BOTTOM_BODY = 3;
    static const int KILL_LEFT_ARM = 4;
    static const int KILL_RIGHT_ARM = 5;
    static const int KILL_TOP_BODY = 6;
    static const int KILL_NECK = 7;
    static const int KILL_HEAD = 8;

    static const int MAX_MOVES = 8;
    static const int MAX_ERRORS = 8;

    string head = HEAD, neck = NECK, rightArm = RIGHT_ARM, leftArm = LEFT_ARM;
    string topBody = TOP_BODY, bottomBody = BOTTOM_BODY;
    string rightFoot = RIGHT_FOOT, leftFoot = LEFT_FOOT;

    string word = "Sunday", tip = "Week day";
    string letters = "";
    int moves = 0, errors = 0;

    string current();

    string getletters();

    void computeErrors();

    void kill();

public:
    void move(string letter);

    bool won();

    bool lose();

    void print();

};

#endif