#include "StringUtil.h"

bool StringUtil::areequals(const string &a, const string &b) {
    unsigned long size = a.size();

    if (b.size() != size) {

        return false;
    } else {

        for (unsigned int i = 0; i < size; i++) {
            if (!areequals(a[i], b[i])) {
                return false;
            }
        }
    }

    return true;
}

bool StringUtil::areequals(char a, char b) {
    return tolower(a) == tolower(b);
}
