#include "Runner.h"

Runner::Runner() {
    this->hangman = new Hangman();
}

Runner::~Runner() {
    delete this->hangman;
}

void Runner::run() {
    string letter;

    while (!this->hangman->won() && !this->hangman->lose()) {
        this->clear();
        this->hangman->print();

        cout << endl << "Type your letter" << endl;
        cin >> letter;

        this->hangman->move(letter);
    }

    this->clear();
    this->hangman->print();

    if (this->hangman->won()) {
        cout << endl << "...You won..." << endl;
    } else {
        cout << endl << "...You lose..." << endl;
    }
}

void Runner::clear() {
    system("clear");
}