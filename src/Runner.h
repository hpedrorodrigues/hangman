#ifndef HANGMAN_RUNNER_H
#define HANGMAN_RUNNER_H

#include "Hangman.h"

class Runner {

private:
    Hangman *hangman;

    void clear();

public:
    Runner();

    void run();

    virtual ~Runner();
};

#endif