#include "Hangman.h"

void Hangman::print() {
    cout << "**" << "************" << endl;
    cout << "**" << setw(12) << "|" << endl;
    cout << "**" << setw(12) << "_" << endl;
    cout << "**" << setw(17) << this->head << endl;
    cout << "**" << setw(12) << this->neck << endl;
    cout << "**" << setw(11) << this->leftArm << this->topBody << this->rightArm << endl;
    cout << "**" << setw(12) << this->bottomBody << endl;
    cout << "**" << setw(11) << this->leftFoot << " " << this->rightFoot << endl;
    cout << "**" << endl;
    cout << "**" << setw(19) << " --> Tip: " << this->tip << endl;
    cout << "**" << setw(29) << " --> Typed letters: " << this->getletters() << endl;
    cout << "**" << endl;
    cout << "**" << endl;
    cout << "**" << setw(20) << " --> Word: " << this->current() << endl;
    cout << "**" << endl;
}

string Hangman::current() {
    string current = "";

    for (register int i = 0; i < this->word.length(); i++) {

        bool exists = false;

        for (register int j = 0; j < this->letters.length(); j++) {
            if (StringUtil::areequals(this->word[i], this->letters[j])) {
                exists = true;
                break;
            }
        }

        if (exists) {
            current += this->word[i];
            current += " ";
        } else {
            current += "_ ";
        }
    }

    return current;
}

void Hangman::move(string letter) {
    this->letters += letter;
    this->computeErrors();
    this->moves--;
    this->kill();
}

void Hangman::kill() {
    switch (this->errors) {
        case KILL:
            break;
        case KILL_LEFT_FOOT:
            this->leftFoot = "";
            break;
        case KILL_RIGHT_FOOT:
            this->rightFoot = "";
            break;
        case KILL_BOTTOM_BODY:
            this->bottomBody = "";
            break;
        case KILL_LEFT_ARM:
            this->leftArm = "";
            break;
        case KILL_RIGHT_ARM:
            this->rightArm = "";
            break;
        case KILL_TOP_BODY:
            this->topBody = "";
            break;
        case KILL_NECK:
            this->neck = "";
            break;
        case KILL_HEAD:
            this->head = "";
            break;
    }
}

bool Hangman::lose() {
    return this->errors >= MAX_ERRORS || this->moves >= MAX_MOVES;
}

bool Hangman::won() {
    string current = this->current();
    current.erase(remove(current.begin(), current.end(), ' '), current.end());

    return !this->lose() && StringUtil::areequals(current, this->word);
}

void Hangman::computeErrors() {
    char letter = this->letters[this->letters.length() - 1];
    bool exists = false;

    for (register int i = 0; i < this->word.length(); i++) {
        if (StringUtil::areequals(this->word[i], letter)) {
            exists = true;
        }
    }

    if (!exists) {
        this->errors++;
    }
}

string Hangman::getletters() {
    string letters = "";

    for (register int i = 0; i < this->letters.length(); i++) {
        letters += this->letters[i];
        letters += " ";
    }

    return letters;
}